package ru.spb.beavers.core.gui;

import ru.spb.beavers.core.data.StorageModules;
import ru.spb.beavers.modules.ITaskModule;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * View главного меню приложения
 */
public class MenuView extends JPanel {

    private final JPanel taskListPanel = new JPanel();

    public MenuView() {
        super(null);
        taskListPanel.setLayout(new BoxLayout(taskListPanel, BoxLayout.PAGE_AXIS));
        JScrollPane scrollPane = new JScrollPane(taskListPanel);
        scrollPane.setSize(300, 400);
        scrollPane.setLocation(300, 50);
        this.add(scrollPane);

        new MenuViewPresenter().initMenu();
    }

    private class MenuViewPresenter {

        /**
         * Производит инициализацию главного меню
         * Выполняет загрузку модулей, их разбор и составление списка доступных для выполнения
         */
        public void initMenu() {
            for (final ITaskModule module : StorageModules.getModules()) {
                JButton btnStartModule = new JButton(module.getTitle());
                btnStartModule.setAlignmentX(CENTER_ALIGNMENT);
                btnStartModule.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent event) {
                        DescriptionView descriptionView = GUIManager.getDescriptionView();
                        descriptionView.removeAll();
                        module.initDescriptionPanel(descriptionView.getDescriptionPanel());
                        GUIManager.getInputView().initSaveButtonListener(module.getPressSaveListener());
                        GUIManager.getInputView().initLoadButtonListener(module.getPressLoadListener());
                        GUIManager.setActiveView(descriptionView);

                        StorageModules.setActiveModule(module);
                    }
                });
                MenuView.this.taskListPanel.add(btnStartModule);
                MenuView.this.taskListPanel.add(Box.createVerticalStrut(3));
            }
        }
    }
}
